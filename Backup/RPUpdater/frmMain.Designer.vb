<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.btnBackground = New System.Windows.Forms.Button
        Me.txtAppPath = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnStartup = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtAppName = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtFTPAddress = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtFTPFolder = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtRarName = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtFTPUsername = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtFTPpassword = New System.Windows.Forms.TextBox
        Me.btnVersions = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtAppUpdatePath = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.lblCurrentVersion = New System.Windows.Forms.Label
        Me.lblUpdateVersion = New System.Windows.Forms.Label
        Me.lblFTPVersion = New System.Windows.Forms.Label
        Me.lstErrors = New System.Windows.Forms.ListBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.btnFTPTest = New System.Windows.Forms.Button
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.chkPassive = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'btnBackground
        '
        Me.btnBackground.Location = New System.Drawing.Point(558, 281)
        Me.btnBackground.Name = "btnBackground"
        Me.btnBackground.Size = New System.Drawing.Size(134, 23)
        Me.btnBackground.TabIndex = 100
        Me.btnBackground.Text = "Background Download"
        Me.btnBackground.UseVisualStyleBackColor = True
        '
        'txtAppPath
        '
        Me.txtAppPath.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAppPath.Location = New System.Drawing.Point(151, 12)
        Me.txtAppPath.Name = "txtAppPath"
        Me.txtAppPath.Size = New System.Drawing.Size(541, 20)
        Me.txtAppPath.TabIndex = 10
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Application Path"
        '
        'btnStartup
        '
        Me.btnStartup.Location = New System.Drawing.Point(558, 321)
        Me.btnStartup.Name = "btnStartup"
        Me.btnStartup.Size = New System.Drawing.Size(134, 23)
        Me.btnStartup.TabIndex = 110
        Me.btnStartup.Text = "Startup Copy"
        Me.btnStartup.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Application Name"
        '
        'txtAppName
        '
        Me.txtAppName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAppName.Location = New System.Drawing.Point(151, 38)
        Me.txtAppName.Name = "txtAppName"
        Me.txtAppName.Size = New System.Drawing.Size(541, 20)
        Me.txtAppName.TabIndex = 20
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(23, 119)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "FTP address"
        '
        'txtFTPAddress
        '
        Me.txtFTPAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFTPAddress.Location = New System.Drawing.Point(151, 116)
        Me.txtFTPAddress.Name = "txtFTPAddress"
        Me.txtFTPAddress.Size = New System.Drawing.Size(541, 20)
        Me.txtFTPAddress.TabIndex = 50
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 145)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "FTP Folder"
        '
        'txtFTPFolder
        '
        Me.txtFTPFolder.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFTPFolder.Location = New System.Drawing.Point(151, 142)
        Me.txtFTPFolder.Name = "txtFTPFolder"
        Me.txtFTPFolder.Size = New System.Drawing.Size(541, 20)
        Me.txtFTPFolder.TabIndex = 60
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(23, 93)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "RAR File Name"
        '
        'txtRarName
        '
        Me.txtRarName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRarName.Location = New System.Drawing.Point(151, 90)
        Me.txtRarName.Name = "txtRarName"
        Me.txtRarName.Size = New System.Drawing.Size(541, 20)
        Me.txtRarName.TabIndex = 40
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(23, 171)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(78, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "FTP Username"
        '
        'txtFTPUsername
        '
        Me.txtFTPUsername.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFTPUsername.Location = New System.Drawing.Point(151, 168)
        Me.txtFTPUsername.Name = "txtFTPUsername"
        Me.txtFTPUsername.Size = New System.Drawing.Size(541, 20)
        Me.txtFTPUsername.TabIndex = 70
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(23, 197)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(76, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "FTP Password"
        '
        'txtFTPpassword
        '
        Me.txtFTPpassword.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFTPpassword.Location = New System.Drawing.Point(151, 194)
        Me.txtFTPpassword.Name = "txtFTPpassword"
        Me.txtFTPpassword.Size = New System.Drawing.Size(541, 20)
        Me.txtFTPpassword.TabIndex = 80
        '
        'btnVersions
        '
        Me.btnVersions.Location = New System.Drawing.Point(558, 240)
        Me.btnVersions.Name = "btnVersions"
        Me.btnVersions.Size = New System.Drawing.Size(134, 23)
        Me.btnVersions.TabIndex = 90
        Me.btnVersions.Text = "Check Versions"
        Me.btnVersions.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(23, 67)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(122, 13)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Application Update Path"
        '
        'txtAppUpdatePath
        '
        Me.txtAppUpdatePath.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAppUpdatePath.Location = New System.Drawing.Point(151, 64)
        Me.txtAppUpdatePath.Name = "txtAppUpdatePath"
        Me.txtAppUpdatePath.Size = New System.Drawing.Size(541, 20)
        Me.txtAppUpdatePath.TabIndex = 30
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(23, 267)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(79, 13)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "Current Version"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(125, 267)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 13)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "Update Version"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(227, 267)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(65, 13)
        Me.Label11.TabIndex = 21
        Me.Label11.Text = "FTP Version"
        '
        'lblCurrentVersion
        '
        Me.lblCurrentVersion.AutoSize = True
        Me.lblCurrentVersion.Location = New System.Drawing.Point(23, 294)
        Me.lblCurrentVersion.Name = "lblCurrentVersion"
        Me.lblCurrentVersion.Size = New System.Drawing.Size(27, 13)
        Me.lblCurrentVersion.TabIndex = 22
        Me.lblCurrentVersion.Text = "N/A"
        '
        'lblUpdateVersion
        '
        Me.lblUpdateVersion.AutoSize = True
        Me.lblUpdateVersion.Location = New System.Drawing.Point(125, 294)
        Me.lblUpdateVersion.Name = "lblUpdateVersion"
        Me.lblUpdateVersion.Size = New System.Drawing.Size(27, 13)
        Me.lblUpdateVersion.TabIndex = 23
        Me.lblUpdateVersion.Text = "N/A"
        '
        'lblFTPVersion
        '
        Me.lblFTPVersion.AutoSize = True
        Me.lblFTPVersion.Location = New System.Drawing.Point(227, 294)
        Me.lblFTPVersion.Name = "lblFTPVersion"
        Me.lblFTPVersion.Size = New System.Drawing.Size(27, 13)
        Me.lblFTPVersion.TabIndex = 24
        Me.lblFTPVersion.Text = "N/A"
        '
        'lstErrors
        '
        Me.lstErrors.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstErrors.FormattingEnabled = True
        Me.lstErrors.ItemHeight = 12
        Me.lstErrors.Location = New System.Drawing.Point(12, 368)
        Me.lstErrors.Name = "lstErrors"
        Me.lstErrors.Size = New System.Drawing.Size(680, 88)
        Me.lstErrors.TabIndex = 111
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(12, 352)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(42, 13)
        Me.Label12.TabIndex = 112
        Me.Label12.Text = "Results"
        '
        'btnFTPTest
        '
        Me.btnFTPTest.Location = New System.Drawing.Point(12, 462)
        Me.btnFTPTest.Name = "btnFTPTest"
        Me.btnFTPTest.Size = New System.Drawing.Size(115, 23)
        Me.btnFTPTest.TabIndex = 113
        Me.btnFTPTest.Text = "Test Cmd Line FTP"
        Me.btnFTPTest.UseVisualStyleBackColor = True
        Me.btnFTPTest.Visible = False
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Text = "AMG : Checking for Updates..."
        Me.NotifyIcon1.Visible = True
        '
        'chkPassive
        '
        Me.chkPassive.AutoSize = True
        Me.chkPassive.Checked = True
        Me.chkPassive.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPassive.Location = New System.Drawing.Point(599, 462)
        Me.chkPassive.Name = "chkPassive"
        Me.chkPassive.Size = New System.Drawing.Size(93, 17)
        Me.chkPassive.TabIndex = 114
        Me.chkPassive.Text = "Passive Mode"
        Me.chkPassive.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(704, 493)
        Me.Controls.Add(Me.chkPassive)
        Me.Controls.Add(Me.btnFTPTest)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.lstErrors)
        Me.Controls.Add(Me.lblFTPVersion)
        Me.Controls.Add(Me.lblUpdateVersion)
        Me.Controls.Add(Me.lblCurrentVersion)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtAppUpdatePath)
        Me.Controls.Add(Me.btnVersions)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtFTPpassword)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtFTPUsername)
        Me.Controls.Add(Me.txtRarName)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtFTPFolder)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtFTPAddress)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtAppName)
        Me.Controls.Add(Me.btnStartup)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtAppPath)
        Me.Controls.Add(Me.btnBackground)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Updater"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnBackground As System.Windows.Forms.Button
    Friend WithEvents txtAppPath As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnStartup As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtAppName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtFTPAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtFTPFolder As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtRarName As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtFTPUsername As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtFTPpassword As System.Windows.Forms.TextBox
    Friend WithEvents btnVersions As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtAppUpdatePath As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblCurrentVersion As System.Windows.Forms.Label
    Friend WithEvents lblUpdateVersion As System.Windows.Forms.Label
    Friend WithEvents lblFTPVersion As System.Windows.Forms.Label
    Friend WithEvents lstErrors As System.Windows.Forms.ListBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btnFTPTest As System.Windows.Forms.Button
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents chkPassive As System.Windows.Forms.CheckBox

End Class
