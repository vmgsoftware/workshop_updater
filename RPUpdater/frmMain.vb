Option Strict On
Option Explicit On

Imports System.IO

Public Class frmMain

    Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
    Public loggedIn As Boolean

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim config As Boolean
        config = True
        Dim args() As String
        args = Split(System.Environment.CommandLine, """") 'split by quotation mark
        'CommandLine gives you the path of the exe in quotes and then the param

        'to extract the param
        If CInt(((args(UBound(args))).IndexOf("-config"))) > 0 Then
            config = True
        End If
        '   config = False ''`
        MsgBox(Application.StartupPath.ToString)
        If Not File.Exists(Application.StartupPath & "\Update\unrar.exe") Then
            MsgBox("Could not locate Unrar.exe in \Update folder", MsgBoxStyle.Exclamation)
            Application.Exit()
        End If

        If Not File.Exists(Application.StartupPath & "\Update\version.dat") Then
            MsgBox("Could not locate Version.dat in \Update folder", MsgBoxStyle.Exclamation)
            Application.Exit()
        End If

        If Not File.Exists(Application.StartupPath & "\version.dat") Then
            MsgBox("Could not locate Version.dat", MsgBoxStyle.Exclamation)
            Application.Exit()
        End If

        If Not (config) Then

            If File.Exists(Application.StartupPath & "\Workshop.accdb") Then
                Dim x As String = Application.StartupPath & "\Workshop.accdb"

                Try

                    Call btnStartup_Click(sender, e)
                    System.Diagnostics.Process.Start(x)
                Catch
                    Application.Exit()
                End Try

                ' spawn thread for updates

                Me.NotifyIcon1.Visible = True

                Call btnBackground_Click(sender, e)


                Me.NotifyIcon1.Visible = False
                Me.Close()
            Else
                MsgBox("Could not locate Workshop.accdb", MsgBoxStyle.Exclamation)
                Application.Exit()
            End If

        Else



            '<LOGIN>
            loggedIn = False
            frmPassword.ShowDialog()

            If loggedIn = False Then
                Me.Dispose()
            End If
            '</LOGIN>

            Me.txtAppPath.Text = Application.StartupPath
            Me.txtAppName.Text = "Workshop.accdb"
            Me.txtAppUpdatePath.Text = String.Concat(Application.StartupPath, "\Update")
            Me.txtRarName.Text = "Workshop.rar"
            Me.txtFTPAddress.Text = "41.204.198.12"
            Me.txtFTPAddress.Text = "ftp.amgauto.co.za"
            Me.txtFTPAddress.Text = "ftp.vmgsoftware.co.za"

            Me.txtFTPFolder.Text = "/vmgsoftware.co.za/wwwroot/WSLiveUpdate"
            Me.txtFTPUsername.Text = "amgauto.co.za"
            Me.txtFTPpassword.Text = "%AmG$#12D34l3r"
            Me.txtFTPUsername.Text = "vmgsoftware.co.za"
            Me.txtFTPpassword.Text = "vmg72ftp"
            Me.txtFTPUsername.Text = "vmgsouvp"
            Me.txtFTPpassword.Text = "g88tyt6E"

        End If


    End Sub

    Private Sub checkVersions()

        Me.lblCurrentVersion.Text = GetFileContents(String.Concat(Me.txtAppPath.Text, "\version.dat"))
        Me.lblUpdateVersion.Text = GetFileContents(String.Concat(Me.txtAppUpdatePath.Text, "\version.dat"))

        If File.Exists(String.Concat(Me.txtAppUpdatePath.Text, "\temp.dat")) Then
            Kill(String.Concat(Me.txtAppUpdatePath.Text, "\temp.dat"))
        End If

        downloadFTPversion()

        Me.lblFTPVersion.Text = GetFileContents(String.Concat(Me.txtAppUpdatePath.Text, "\temp.dat"))
    End Sub

    Private Sub downloadFTPversionBG(ByVal RFile As String, ByVal LFile As String)

        If IsConnectionAvailable() Then
            Dim localFile As String = String.Concat(String.Concat(Application.StartupPath, "\Update"), "\" & LFile & "")
            Dim remoteFile As String = String.Concat("/vmgsoftware.co.za/wwwroot/WSLiveUpdate", "/" & RFile & "")
            '   Dim host As String = String.Concat("ftp://", "41.204.198.12")
            Dim host As String = String.Concat("ftp://", "ftp.vmgsoftware.co.za")
            Dim username As String = "amgauto.co.za"
            Dim password As String = "%AmG$#12D34l3r"
            username = "vmgsoftware.co.za"
            password = "vmg72ftp"
            username = "vmgsouvp"
            password = "g88tyt6E"

            Dim URI As String = host & remoteFile
            Dim ftp As System.Net.FtpWebRequest = _
                CType(Net.FtpWebRequest.Create(URI), Net.FtpWebRequest)

            ftp.Credentials = New System.Net.NetworkCredential(username, password)

            ftp.KeepAlive = False

            ftp.UseBinary = True
            Me.lstErrors.Items.Add("Passive Mode : " & CStr(Me.chkPassive.Checked))
            ftp.UsePassive = Me.chkPassive.Checked

            ftp.Method = System.Net.WebRequestMethods.Ftp.DownloadFile
            Try
                Using response As System.Net.FtpWebResponse = _
                CType(ftp.GetResponse, System.Net.FtpWebResponse)
                    Using responseStream As IO.Stream = response.GetResponseStream

                        Using fs As New IO.FileStream(localFile, IO.FileMode.Create)
                            Dim buffer(2047) As Byte
                            Dim read As Integer = 0
                            Do
                                read = responseStream.Read(buffer, 0, buffer.Length)
                                fs.Write(buffer, 0, read)
                            Loop Until read = 0
                            responseStream.Close()
                            fs.Flush()
                            fs.Close()
                        End Using
                        responseStream.Close()
                    End Using
                    response.Close()
                End Using
            Catch
                Me.lstErrors.Items.Add("An Error Ocurred while trying to retrieve version information")
                MsgBox(Err.Description)
            End Try
        Else

            Me.lstErrors.Items.Add("Internet Connection not found")
        End If


    End Sub

    Private Sub downloadFTPversion()

        If IsConnectionAvailable() Then
            Dim localFile As String = String.Concat(Me.txtAppUpdatePath.Text, "\temp.dat")
            Dim remoteFile As String = String.Concat(Me.txtFTPFolder.Text, "/version.dat")
            Dim host As String = String.Concat("ftp://", Me.txtFTPAddress.Text)
            Dim username As String = Me.txtFTPUsername.Text
            Dim password As String = Me.txtFTPpassword.Text

            Dim URI As String = host & remoteFile
            Dim ftp As System.Net.FtpWebRequest = _
                CType(Net.FtpWebRequest.Create(URI), Net.FtpWebRequest)

            ftp.Credentials = New System.Net.NetworkCredential(username, password)

            ftp.KeepAlive = False

            ftp.UseBinary = True

            Me.lstErrors.Items.Add("Passive Mode : " & CStr(Me.chkPassive.Checked))
            ftp.UsePassive = Me.chkPassive.Checked

            ftp.Method = System.Net.WebRequestMethods.Ftp.DownloadFile
            Try
                Using response As System.Net.FtpWebResponse = _
                CType(ftp.GetResponse, System.Net.FtpWebResponse)
                    Using responseStream As IO.Stream = response.GetResponseStream

                        Using fs As New IO.FileStream(localFile, IO.FileMode.Create)
                            Dim buffer(2047) As Byte
                            Dim read As Integer = 0
                            Do
                                read = responseStream.Read(buffer, 0, buffer.Length)
                                fs.Write(buffer, 0, read)
                            Loop Until read = 0
                            responseStream.Close()
                            fs.Flush()
                            fs.Close()
                        End Using
                        responseStream.Close()
                    End Using
                    response.Close()
                End Using
            Catch
                Me.lstErrors.Items.Add("An Error Ocurred while trying to retrieve version information")
            End Try
        Else

            Me.lstErrors.Items.Add("Internet Connection not found")
        End If


    End Sub

    Private Sub btnVersions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVersions.Click
        Try
            Me.btnVersions.Enabled = False
            Application.DoEvents()
            Application.DoEvents()
            checkVersions()
            Application.DoEvents()
            Me.lstErrors.Items.Add("Versions Done")

        Catch ex As Exception
            Me.lstErrors.Items.Add(Err.Description)
        Finally
            Me.btnVersions.Enabled = True
        End Try
    End Sub

    Private Sub btnBackground_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackground.Click
        Try
            Me.btnBackground.Enabled = False
            Application.DoEvents()
            Application.DoEvents()
            If File.Exists(String.Concat(Application.StartupPath & "\Update", "\temp.dat")) Then
                Kill(String.Concat(Application.StartupPath & "\Update", "\temp.dat"))
            End If

            downloadFTPversionBG("version.dat", "temp.dat")

            Me.lblUpdateVersion.Text = GetFileContents(String.Concat(Application.StartupPath & "\Update", "\version.dat"))
            Me.lblFTPVersion.Text = GetFileContents(String.Concat(Application.StartupPath & "\Update", "\temp.dat"))

            If Val(Me.lblFTPVersion.Text) > Val(Me.lblUpdateVersion.Text) Then


                If File.Exists(String.Concat(Application.StartupPath & "\Update", "\version.dat")) Then
                    Kill(String.Concat(Application.StartupPath & "\Update", "\version.dat"))
                End If

                If File.Exists(String.Concat(Application.StartupPath & "\Update", "\Workshop.accdb")) Then
                    Kill(String.Concat(Application.StartupPath & "\Update", "\Workshop.accdb"))
                End If

                If File.Exists(String.Concat(Application.StartupPath & "\Update", "\Workshop.rar")) Then
                    Kill(String.Concat(Application.StartupPath & "\Update", "\Workshop.rar"))
                End If


                downloadFTPversionBG("Workshop.rar", "Workshop.rar")
                downloadFTPversionBG("version.dat", "version.dat")

                Shell("""" & String.Concat(Application.StartupPath & "\Update", "\unrar.exe") & """" & " e -o+ " & """" & Application.StartupPath & "\Update\" & "Workshop.rar" & _
                """" & " " & """" & Application.StartupPath & "\Update\" & """", AppWinStyle.Hide, True)

            End If


            Me.lstErrors.Items.Add("Background Done")
        Catch
            Me.lstErrors.Items.Add(Err.Description)
        Finally
            Me.btnBackground.Enabled = True
        End Try


    End Sub

    Private Sub btnStartup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStartup.Click

        Try
            Me.btnStartup.Enabled = False
            Application.DoEvents()
            Application.DoEvents()
            Me.lblCurrentVersion.Text = GetFileContents(String.Concat(Application.StartupPath, "\version.dat"))
            Me.lblUpdateVersion.Text = GetFileContents(String.Concat(Application.StartupPath & "\Update", "\version.dat"))

            If IsNumeric(Me.lblCurrentVersion.Text) AndAlso IsNumeric(Me.lblUpdateVersion.Text) Then
                If Val(Me.lblUpdateVersion.Text) > Val(Me.lblCurrentVersion.Text) Then
                    Try

                        System.IO.File.Copy(String.Concat(Application.StartupPath & "\Update", "\version.dat"), String.Concat(Application.StartupPath, "\version.dat"), True)
                        System.IO.File.Copy(String.Concat(Application.StartupPath & "\Update", "\Workshop.accdb"), String.Concat(Application.StartupPath, "\Workshop.accdb"), True)

                    Catch
                        Me.lstErrors.Items.Add(Err.Description)
                    End Try
                End If
            Else
                Me.lstErrors.Items.Add("Numeric values expected in Version files")
            End If
            Me.lstErrors.Items.Add("Startup Done")
        Catch
            Me.lstErrors.Items.Add(Err.Description)
        Finally
            Me.btnStartup.Enabled = True
        End Try

    End Sub


End Class
